/**
 * /* eslint-disable react-hooks/exhaustive-deps
 *
 * @format
 */
/* eslint-disable react-hooks/exhaustive-deps*/

import React from 'react';
import { connect } from 'react-redux';
import NavBarComponent from '../components/common/NavBarComponent';
import { Container, Box, Grid } from '@mui/material';
import Toast from '../components/common/Toast';
import * as authAction from '../actions/AuthAction';
import * as postAction from '../actions/PostAction';
import * as userAction from '../actions/UserAction';
import * as commentAction from '../actions/CommentAction';
import Post from '../components/common/Post';
import PostForm from '../components/common/PostForm';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import PeopleAltIcon from '@mui/icons-material/PeopleAlt';
import BookmarkIcon from '@mui/icons-material/Bookmark';
import GroupsIcon from '@mui/icons-material/Groups';
import SportsEsportsIcon from '@mui/icons-material/SportsEsports';
import useClasses from '../assets/styles/UseClasses';
import { styles } from '../assets/styles/HomeStyles';
export const HomeContainer = (props) => {
  const classes = useClasses(styles);
  const [ShowToast, setShowToast] = React.useState(false);
  const [selectedPost, setSelectedPost] = React.useState();

  React.useEffect(() => {
    if (!ShowToast && (props.errMsg || props.ssMsg)) {
      setShowToast(true);
      setTimeout(() => {
        props.clearMsg();
      }, 6 * 1000);
    }
  }, [props]);

  // React.useEffect(() => {
  //   props.getCsrfToken();
  // }, []);

  React.useEffect(() => {
    props.getAllPost({ activePage: 1 });
  }, []);

  return (
    <>
      {ShowToast ? (
        <Toast
          setShowToast={(data) => setShowToast(data)}
          msg={props.error ? props.errMsg : props.ssMsg}
          error={props.error}
        />
      ) : null}
      <NavBarComponent
        logoutRequest={() => props.logoutRequest()}
        listUser={props.listUser}
        getSingleUserRequest={(data) => props.getSingleUserRequest(data)}
        searchRequest={(data) => props.searchRequest(data)}
        id={props?.userDetail?._id}
        userDetail={props?.userDetail}
      />
      <Container>
        <Grid item sm={3}>
          <Box sx={classes.sxbox_left}>
            <Box sx={classes.sxitem}>
              <Box>
                <AccountCircleIcon />
                <span sx={classes.sxname}> Nguyễn Thái</span>
              </Box>
            </Box>
            <Box sx={classes.sxitem}>
              <Box>
                <PeopleAltIcon />
                <span sx={classes.sxname}>Bạn bè</span>
              </Box>
            </Box>
            <Box sx={classes.sxitem}>
              <Box>
                <BookmarkIcon />
                <span sx={classes.sxname}> Đã lưu</span>
              </Box>
            </Box>
            <Box sx={classes.sxitem}>
              <Box>
                <GroupsIcon />
                <span sx={classes.sxname}> Nhóm</span>
              </Box>
            </Box>
            <Box sx={classes.sxitem}>
              <Box>
                <SportsEsportsIcon />
                <span sx={classes.sxname}> Chơi game</span>
              </Box>
            </Box>
            <Box>
              <Box style={{ fontSize: '20px', fontWeight: 600 }}>
                Lỗi tắt của bạn
              </Box>
              <Box sx={classes.sxitem}>
                <Box>
                  <AccountCircleIcon />
                  <span sx={classes.sxname}> Nhóm tấu hài</span>
                </Box>
              </Box>
              <Box sx={classes.sxitem}>
                <Box>
                  <AccountCircleIcon />
                  <span sx={classes.sxname}> Nhóm tấu hài</span>
                </Box>
              </Box>
              <Box sx={classes.sxitem}>
                <Box>
                  <AccountCircleIcon />
                  <span sx={classes.sxname}> Nhóm tấu hài</span>
                </Box>
              </Box>
              <Box sx={classes.sxitem}>
                <Box>
                  <AccountCircleIcon />
                  <span sx={classes.sxname}> Nhóm tấu hài</span>
                </Box>
              </Box>
            </Box>
          </Box>
        </Grid>
        <Grid item sm={6}>
          <Box sx={classes.sxbox_center}>
            <div
            // sx={
            //   'mx-auto flex-col justify-center max-w-1/2 xl:max-w-1/2 2xl:max-w-2/5 '
            // }
            >
              <PostForm
                avatar={props?.userDetail?.avatar}
                fullname={props?.userDetail?.fullname}
                selectedPost={selectedPost}
                setSelectedPost={setSelectedPost}
                addPost={props.addPost}
                updatePost={props.updatePost}
              />
              {props?.listPost &&
                props?.listPost.map((post, index) => {
                  return (
                    <Post
                      key={index}
                      post={post}
                      setSelectedPost={setSelectedPost}
                      likePost={props.likePost}
                      commentPost={props.commentPost}
                      deletePost={props.deletePost}
                      updateComment={props.updateComment}
                      deleteComment={props.deleteComment}
                    />
                  );
                })}
            </div>
          </Box>
        </Grid>
        <Grid item sm={3}>
          <Box sx={classes.sxbox_right}>
            <Box>
              <Box style={{ fontSize: '20px', fontWeight: 600 }}>
                Người liên hệ
              </Box>
            </Box>
            <Box sx={classes.sxitem}>
              <Box>
                <AccountCircleIcon />
                <span sx={classes.sxname}> Nguyễn Văn A</span>
              </Box>
            </Box>
            <Box sx={classes.sxitem}>
              <Box>
                <AccountCircleIcon />
                <span sx={classes.sxname}> Nguyễn Văn A</span>
              </Box>
            </Box>
            <Box sx={classes.sxitem}>
              <Box>
                <AccountCircleIcon />
                <span sx={classes.sxname}> Nguyễn Văn A</span>
              </Box>
            </Box>
            <Box sx={classes.sxitem}>
              <Box>
                <AccountCircleIcon />
                <span sx={classes.sxname}> Nguyễn Văn A</span>
              </Box>
            </Box>
            <Box sx={classes.sxitem}>
              <Box>
                <AccountCircleIcon />
                <span sx={classes.sxname}> Nguyễn Văn A</span>
              </Box>
            </Box>
            <Box sx={classes.sxitem}>
              <Box>
                <AccountCircleIcon />
                <span sx={classes.sxname}> Nguyễn Văn A</span>
              </Box>
            </Box>
            <Box sx={classes.sxitem}>
              <Box>
                <AccountCircleIcon />
                <span sx={classes.sxname}> Nguyễn Văn A</span>
              </Box>
            </Box>
            <Box sx={classes.sxitem}>
              <Box>
                <AccountCircleIcon />
                <span sx={classes.sxname}> Nguyễn Văn A</span>
              </Box>
            </Box>
            <Box sx={classes.sxitem}>
              <Box>
                <AccountCircleIcon />
                <span sx={classes.sxname}> Nguyễn Văn A</span>
              </Box>
            </Box>
            <Box sx={classes.sxitem}>
              <Box>
                <AccountCircleIcon />
                <span sx={classes.sxname}> Nguyễn Văn A</span>
              </Box>
            </Box>
            <Box sx={classes.sxitem}>
              <Box>
                <AccountCircleIcon />
                <span sx={classes.sxname}> Nguyễn Văn A</span>
              </Box>
            </Box>
            <Box sx={classes.sxitem}>
              <Box>
                <AccountCircleIcon />
                <span sx={classes.sxname}> Nguyễn Văn A</span>
              </Box>
            </Box>
            <Box sx={classes.sxitem}>
              <Box>
                <AccountCircleIcon />
                <span sx={classes.sxname}> Nguyễn Văn A</span>
              </Box>
            </Box>
            <Box sx={classes.sxitem}>
              <Box>
                <AccountCircleIcon />
                <span sx={classes.sxname}> Nguyễn Văn A</span>
              </Box>
            </Box>
            <Box sx={classes.sxitem}>
              <Box>
                <AccountCircleIcon />
                <span sx={classes.sxname}> Nguyễn Văn A</span>
              </Box>
            </Box>
            <Box>
              <Box style={{ fontSize: '20px', fontWeight: 600 }}>
                Cuộc trò chuyện nhóm
              </Box>
              <Box sx={classes.sxitem}>
                <Box>
                  <AccountCircleIcon />
                  <span sx={classes.sxname}> Nhóm tấu hài</span>
                </Box>
              </Box>
            </Box>
          </Box>
        </Grid>
        {/* <Box sx={classes.sxcontainer}> */}
        {/* </Box> */}
      </Container>
    </>
  );
};

const mapStateToProps = (state) => ({
  listPost: state.post.listPost,
  userDetail: state.user.userDetail,
  listUser: state.user.listUser,
  error: state.auth.error,
  errMsg: state.auth.errMsg,
  ssMsg: state.auth.ssMsg,
});

const mapDispatchToProps = (dispatch) => ({
  logoutRequest: () => dispatch(authAction.logoutRequest()),
  clearMsg: () => dispatch(authAction.clearMsg()),
  searchRequest: (data) => dispatch(userAction.searchRequest(data)),
  getSingleUserRequest: (data) =>
    dispatch(userAction.getSingleUser.request(data)),
  getCsrfToken: () => dispatch(authAction.getCsrfToken()),
  getAllPost: (data) => dispatch(postAction.getAllPost.request(data)),
  likePost: (data) => dispatch(postAction.likePost.request(data)),
  addPost: (data) => dispatch(postAction.addPost.request(data)),
  updatePost: (data) => dispatch(postAction.updatePost.request(data)),
  deletePost: (data) => dispatch(postAction.deletePost.request(data)),
  commentPost: (data) => dispatch(postAction.commentPost.request(data)),
  updateComment: (data) => dispatch(commentAction.updateComment.request(data)),
  deleteComment: (data) => dispatch(commentAction.deleteComment.request(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(HomeContainer);
