/** @format */

import React from 'react';
import { connect } from 'react-redux';
import ShopViewComponent from '../../components/View/ShopViewComponent';
import ListProducts from '../../components/View/ListProduct';
import { Container } from '@mui/material';
export const ShopView = (props) => {
  const [openCart, setOpenCart] = React.useState(false);
  const [listCart, setListCart] = React.useState();
  return (
    <div>
      <ShopViewComponent
        openCart={openCart}
        setOpenCart={setOpenCart}
        listCart={listCart}
        setListCart={setListCart}
      />
      <ListProducts />
    </div>
  );
};

const mapStateToProps = (state) => ({});

const mapDispatchToProps = (dispatch) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(ShopView);
