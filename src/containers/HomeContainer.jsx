/**
 * /* eslint-disable react-hooks/exhaustive-deps
 *
 * @format
 */
/* eslint-disable react-hooks/exhaustive-deps*/

import React from 'react'
import { connect } from 'react-redux'
import NavBarComponent from '../components/common/NavBarComponent'
import { Container, Box, Grid } from '@mui/material'
import Toast from '../components/common/Toast'
import * as authAction from '../actions/AuthAction'
import * as postAction from '../actions/PostAction'
import * as userAction from '../actions/UserAction'
import * as commentAction from '../actions/CommentAction'
import Post from '../components/common/Post'
import PostForm from '../components/common/PostForm'
import useClasses from '../assets/styles/UseClasses'
import { styles } from '../assets/styles/HomeStyles'
import UserContact from '../components/common/UserContact'
import UserGroup from '../components/common/userGroup'
import Avatar from '../assets/images/ship.png'
import Avatar1 from '../assets/images/cafe2.jpg'
const mockupDta = [
  {
    img: 'https://www.google.com/url?sa=i&url=https%3A%2F%2Fitcafe.vn%2Fanh-gai-xinh%2F&psig=AOvVaw1Np8pFHJxWlLtoSYam0UUa&ust=1635676889493000&source=images&cd=vfe&ved=0CAsQjRxqFwoTCJDZy7H58fMCFQAAAAAdAAAAABAJ',
    id: 1,
    name: 'Nguyễn văn A',
  },
  {
    img: Avatar1,
    id: 2,
    name: 'Nguyễn văn B',
  },
  {
    img: Avatar,
    id: 3,
    name: 'Nguyễn văn c',
  },
  {
    img: Avatar1,
    id: 4,
    name: 'Nguyễn văn D',
  },
  {
    img: Avatar,
    id: 5,
    name: 'Nguyễn văn E',
  },
]
const mockupDtaLeft = [
  {
    img: 'https://static.xx.fbcdn.net/rsrc.php/v3/y2/r/HBcx-giUZ2Y.png',
    id: 1,
    name: 'Nguyễn Đô',
  },
  {
    img: 'https://static.xx.fbcdn.net/rsrc.php/v3/yx/r/-XF4FQcre_i.png',
    id: 2,
    name: 'Friends',
  },
  {
    img: 'https://static.xx.fbcdn.net/rsrc.php/v3/yr/r/2uPlV4oORjU.png',
    id: 3,
    name: 'Saved',
  },
  {
    img: 'https://static.xx.fbcdn.net/rsrc.php/v3/yD/r/mk4dH3FK0jT.png',
    id: 4,
    name: 'Groups',
  },
  {
    img: 'https://static.xx.fbcdn.net/rsrc.php/v3/yn/r/XEWvxf1LQAG.png',
    id: 5,
    name: 'Play games',
  },
]

export const HomeContainer = (props) => {
  const classes = useClasses(styles)
  const [ShowToast, setShowToast] = React.useState(false)
  const [selectedPost, setSelectedPost] = React.useState()

  React.useEffect(() => {
    if (!ShowToast && (props.errMsg || props.ssMsg)) {
      setShowToast(true)
      setTimeout(() => {
        props.clearMsg()
      }, 6 * 1000)
    }
  }, [props])

  // React.useEffect(() => {
  //   props.getCsrfToken();
  // }, []);

  React.useEffect(() => {
    props.getAllPost({ activePage: 1 })
  }, [])

  return (
    <>
      {ShowToast ? (
        <Toast
          setShowToast={(data) => setShowToast(data)}
          msg={props.error ? props.errMsg : props.ssMsg}
          error={props.error}
        />
      ) : null}
      <NavBarComponent
        logoutRequest={() => props.logoutRequest()}
        listUser={props.listUser}
        getSingleUserRequest={(data) => props.getSingleUserRequest(data)}
        searchRequest={(data) => props.searchRequest(data)}
        id={props?.userDetail?._id}
        userDetail={props?.userDetail}
      />
      <Container maxWidth='xxl'>
        <Grid container>
          <Grid item sm={3}>
            <Grid item sm={12}>
              <Box
                sx={{
                  width: '90%',
                  height: '100vh',
                  overflow: 'auto',
                  marginTop: '5%',
                }}
              >
                {mockupDtaLeft.map((user) => (
                  <UserGroup item={user} key={user.id} />
                ))}
              </Box>
            </Grid>
          </Grid>
          <Grid item sm={6}>
            <Box>
              <PostForm
                avatar={props?.userDetail?.avatar}
                fullname={props?.userDetail?.fullname}
                selectedPost={selectedPost}
                setSelectedPost={setSelectedPost}
                addPost={props.addPost}
                updatePost={props.updatePost}
              />
              {props?.listPost &&
                props?.listPost.map((post, index) => {
                  return (
                    <Post
                      key={index}
                      post={post}
                      setSelectedPost={setSelectedPost}
                      likePost={props.likePost}
                      commentPost={props.commentPost}
                      deletePost={props.deletePost}
                      updateComment={props.updateComment}
                      deleteComment={props.deleteComment}
                    />
                  )
                })}
            </Box>
          </Grid>
          <Grid item sm={1}></Grid>
          <Grid item sm={2}>
            <Grid item sm={12}>
              <Box sx={classes.sxTitle}>Contacts</Box>
            </Grid>
            <Grid item sm={12}>
              {mockupDta.map((user) => (
                <UserContact item={user} key={user.id} />
              ))}
            </Grid>
            {/* <Grid item sm={12}>
              <Box sx={classes.sxTitle}> Cuộc trò chuyện nhóm</Box>
            </Grid>
            <Grid item sm={12}>
              {mockupDta.map((user) => (
                <UserContact item={user} key={user.id} />
              ))}
            </Grid>
            <Grid item sm={12}>
              <Box sx={classes.sxTitle}> Trang của bạn</Box>
            </Grid>
            <Grid item sm={12}>
              {mockupDta.map((user) => (
                <UserContact item={user} key={user.id} />
              ))}
            </Grid> */}
          </Grid>
        </Grid>
      </Container>
    </>
  )
}

const mapStateToProps = (state) => ({
  listPost: state.post.listPost,
  userDetail: state.user.userDetail,
  listUser: state.user.listUser,
  error: state.auth.error,
  errMsg: state.auth.errMsg,
  ssMsg: state.auth.ssMsg,
})

const mapDispatchToProps = (dispatch) => ({
  logoutRequest: () => dispatch(authAction.logoutRequest()),
  clearMsg: () => dispatch(authAction.clearMsg()),
  searchRequest: (data) => dispatch(userAction.searchRequest(data)),
  getSingleUserRequest: (data) =>
    dispatch(userAction.getSingleUser.request(data)),
  getCsrfToken: () => dispatch(authAction.getCsrfToken()),
  getAllPost: (data) => dispatch(postAction.getAllPost.request(data)),
  likePost: (data) => dispatch(postAction.likePost.request(data)),
  addPost: (data) => dispatch(postAction.addPost.request(data)),
  updatePost: (data) => dispatch(postAction.updatePost.request(data)),
  deletePost: (data) => dispatch(postAction.deletePost.request(data)),
  commentPost: (data) => dispatch(postAction.commentPost.request(data)),
  updateComment: (data) => dispatch(commentAction.updateComment.request(data)),
  deleteComment: (data) => dispatch(commentAction.deleteComment.request(data)),
})

export default connect(mapStateToProps, mapDispatchToProps)(HomeContainer)
