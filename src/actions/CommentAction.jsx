/** @format */

import * as constants from '../constants';
import { createAction } from 'redux-actions';

const createActionSet = (type) => {
  return {
    request: createAction(type.REQUEST),
    success: createAction(type.SUCCESS),
    failure: createAction(type.FAILURE),
  };
};

export const deleteComment = createActionSet(constants.DELETE_COMMENT);
export const updateComment = createActionSet(constants.UPDATE_COMMENT);
