/** @format */

import React from 'react';
import ShopView from '../containers/View/ShopView';

function ShopPage() {
  return (
    <div>
      <ShopView />
    </div>
  );
}

export default ShopPage;
