/** @format */

import React from 'react';
import logo from '../../assets/images/meo-network-logo(1).png';
import cartImg from '../../assets/images/Cart.jpg';
import bgImg from '../../assets/images/back-ground.jpg';
import './voucher.css';

function Voucher() {
  return (
    <div
      style={{
        backgroundImage: `url(${bgImg})`,
      }}
    >
      <a href='/' class='logo'>
        <img src={logo} alt='' style={{ width: 50, height: 50 }} />
        <span></span>
      </a>
      <h6>
        <b>MEOWSTORE</b>
      </h6>

      <div class='navbar'>
        <ul>
          <li>
            <a href='/'>Menu</a>
          </li>
          <li>
            <a href='/'>Trending</a>
          </li>
          <li>
            <a href='/'>Voucher</a>
          </li>
          <li>
            <a href='/'>Cart</a>
          </li>
        </ul>
      </div>
      <a href='/' class='notification'>
        <img src={cartImg} alt='' style={{ width: 50, height: 50 }} />
        <span></span>
      </a>

      <div class='container'>
        <div class='container_1'>
          <div class='col'>
            <div class='card'>
              <img
                src='https://afamilycdn.com/zoom/640_400/2019/11/11/discount-promotion-coupon-in-vintage-design-sale-vector-4538258-15734617977181495504522-crop-15734618079191432528857.jpg'
                class='card-img-top'
                alt='...'
              />
              <div class='card-body'>
                <h5 class='card-title'>Discount 20%</h5>
                <p class='card-text'>Coin conversion: 100 coin</p>
                <p class='card-text'>Expire: 22/8/2021 - 30/9/2021</p>
                <p class='card-text'>Code: asasasasas</p>
                <button class='btt'>Convert</button>
              </div>
            </div>
          </div>
          <div class='col'>
            <div class='card'>
              <img
                src='https://afamilycdn.com/zoom/640_400/2019/11/11/discount-promotion-coupon-in-vintage-design-sale-vector-4538258-15734617977181495504522-crop-15734618079191432528857.jpg'
                class='card-img-top'
                alt='...'
              />
              <div class='card-body'>
                <h5 class='card-title'>Discount 50%</h5>
                <p class='card-text'>Coin conversion: 250 coin</p>
                <p class='card-text'>Expire: 22/8/2021 - 30/9/2021</p>
                <p class='card-text'>Code: asasasasas</p>
                <button class='btt'>Convert</button>
              </div>
            </div>
          </div>
          <div class='col'>
            <div class='card'>
              <img
                src='https://afamilycdn.com/zoom/640_400/2019/11/11/discount-promotion-coupon-in-vintage-design-sale-vector-4538258-15734617977181495504522-crop-15734618079191432528857.jpg'
                class='card-img-top'
                alt='...'
              />
              <div class='card-body'>
                <h5 class='card-title'>Discount 50.000đ</h5>
                <p class='card-text'>Coin conversion: 300 coin</p>
                <p class='card-text'>Expire: 22/8/2021 - 30/9/2021</p>
                <p class='card-text'>Code: asasasasas</p>
                <button class='btt'>Convert</button>
              </div>
            </div>
          </div>
          <div class='col'>
            <div class='card'>
              <img
                src='https://afamilycdn.com/zoom/640_400/2019/11/11/discount-promotion-coupon-in-vintage-design-sale-vector-4538258-15734617977181495504522-crop-15734618079191432528857.jpg'
                class='card-img-top'
                alt='...'
              />
              <div class='card-body'>
                <h5 class='card-title'>Discount 100%</h5>
                <p class='card-text'>Coin conversion: 500 coin</p>
                <p class='card-text'>Expire: 22/8/2021 - 30/9/2021</p>
                <p class='card-text'>Code: asasasasas</p>
                <button class='btt'>Convert</button>
              </div>
            </div>
          </div>
          <div class='col'>
            <div class='card'>
              <img
                src='https://afamilycdn.com/zoom/640_400/2019/11/11/discount-promotion-coupon-in-vintage-design-sale-vector-4538258-15734617977181495504522-crop-15734618079191432528857.jpg'
                class='card-img-top'
                alt='...'
              />
              <div class='card-body'>
                <h5 class='card-title'>Discount 30%</h5>
                <p class='card-text'>Coin conversion: 150 coin</p>
                <p class='card-text'>Expire: 22/8/2021 - 30/9/2021</p>
                <p class='card-text'>Code: asasasasas</p>
                <button class='btt'>Convert</button>
              </div>
            </div>
          </div>
          <div class='col'>
            <div class='card'>
              <img
                src='https://afamilycdn.com/zoom/640_400/2019/11/11/discount-promotion-coupon-in-vintage-design-sale-vector-4538258-15734617977181495504522-crop-15734618079191432528857.jpg'
                class='card-img-top'
                alt='...'
              />
              <div class='card-body'>
                <h5 class='card-title'>Discount 75%</h5>
                <p class='card-text'>Coin conversion: 400 coin</p>
                <p class='card-text'>Expire: 22/8/2021 - 30/9/2021</p>
                <p class='card-text'>Code: asasasasas</p>
                <button class='btt'>Convert</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Voucher;
