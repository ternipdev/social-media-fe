/** @format */

import React from 'react';
import { Grid, Box, Container, Typography, Button } from '@mui/material';
import CaffeNew from '../../assets/images/caffeNew.jpg';
import GroupedButtons from '../../components/common/buttonGroup';
import useClass from '../../assets/styles/UseClasses';
import { styles } from './ListProductStyle';
export default function ListProduct() {
  const classes = useClass(styles);
  return (
    <>
      <Container>
        <Box sx={classes.sxtitleProduct}>Danh sách sản phẩm</Box>
        <Grid container spacing={2}>
          <Grid item lg={4}>
            <Box sx={classes.sxProduct}>
              <img sx={classes.sximage} src={CaffeNew} width='100%' />
            </Box>
            <Typography sx={classes.sxtitle}>coffee house</Typography>
            <Box sx={classes.sxpriceProduct}>
              <Box sx={classes.sxleft}>50$ / cốc</Box>
              <Box sx={classes.sxright}>
                <GroupedButtons />
              </Box>
            </Box>
            <Button sx={classes.sxbutton} variant='contained'>
              Add to cart
            </Button>
          </Grid>
          <Grid item lg={4}>
            <Box sx={classes.sxProduct}>
              <img sx={classes.sximage} src={CaffeNew} width='100%' />
            </Box>
            <Typography sx={classes.sxtitle}>coffee house</Typography>
            <Box sx={classes.sxpriceProduct}>
              <Box sx={classes.sxleft}>50$ / cốc</Box>
              <Box sx={classes.sxright}>
                <GroupedButtons />
              </Box>
            </Box>
            <Button sx={classes.sxbutton} variant='contained'>
              Add to cart
            </Button>
          </Grid>
          <Grid item lg={4}>
            <Box sx={classes.sxProduct}>
              <img sx={classes.sximage} src={CaffeNew} width='100%' />
            </Box>
            <Typography sx={classes.sxtitle}>coffee house</Typography>
            <Box sx={classes.sxpriceProduct}>
              <Box sx={classes.sxleft}>50$ / cốc</Box>
              <Box sx={classes.sxright}>
                <GroupedButtons />
              </Box>
            </Box>
            <Button sx={classes.sxbutton} variant='contained'>
              Add to cart
            </Button>
          </Grid>
          <Grid item lg={4}>
            <Box sx={classes.sxProduct}>
              <img sx={classes.sximage} src={CaffeNew} width='100%' />
            </Box>
            <Typography sx={classes.sxtitle}>coffee house</Typography>
            <Box sx={classes.sxpriceProduct}>
              <Box sx={classes.sxleft}>50$ / cốc</Box>
              <Box sx={classes.sxright}>
                <GroupedButtons />
              </Box>
            </Box>
            <Button sx={classes.sxbutton} variant='contained'>
              Add to cart
            </Button>
          </Grid>
          <Grid item lg={4}>
            <Box sx={classes.sxProduct}>
              <img sx={classes.sximage} src={CaffeNew} width='100%' />
            </Box>
            <Typography sx={classes.sxtitle}>coffee house</Typography>
            <Box sx={classes.sxpriceProduct}>
              <Box sx={classes.sxleft}>50$ / cốc</Box>
              <Box sx={classes.sxright}>
                <GroupedButtons />
              </Box>
            </Box>
              <Button sx={classes.sxbutton} variant='contained'>
                Add to cart
              </Button>
          </Grid>
          <Grid item lg={4}>
            <Box sx={classes.sxProduct}>
              <img sx={classes.sximage} src={CaffeNew} width='100%' />
            </Box>
            <Typography sx={classes.sxtitle}>coffee house</Typography>
            <Box sx={classes.sxpriceProduct}>
              <Box sx={classes.sxleft}>50$ / cốc</Box>
              <Box sx={classes.sxright}>
                <GroupedButtons />
              </Box>
            </Box>
            <Button sx={classes.sxbutton} variant='contained'>
              Add to cart
            </Button>
          </Grid>
        </Grid>
      </Container>
    </>
  );
}
