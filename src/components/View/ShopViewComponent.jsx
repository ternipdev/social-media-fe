/** @format */

import {
  Divider,
  IconButton,
  List,
  ListItem,
  ListItemAvatar,
  ListItemIcon,
  Avatar,
  ListItemText,
  Typography as Typo,
  Drawer,
  TextField,
  Container,
  Grid,
} from '@mui/material'
import { Box } from '@mui/material'
import React from 'react'
import bgImg from '../../assets/images/back-ground.jpg'
import logo from '../../assets/images/meo-network-logo(1).png'
import cart from '../../assets/images/Cart.jpg'
import './voucher.css'
import useClasses from '../../assets/styles/UseClasses'
import { styles } from './ShowViewComponentStyle'
import { Slider } from './ShopViewFragment'
import ListProducts from './ListProduct'
function ShopViewComponent({ listCart, setListCart, openCart, setOpenCart }) {
  const classes = useClasses(styles)

  const toggleDrawer = (event) => {
    if (
      event.type === 'keydown' &&
      (event.key === 'Tab' || event.key === 'Shift')
    ) {
      return
    }

    setOpenCart(true)
  }

  const list = () => (
    <Box role='presentation'>
      <List>
        {[
          {
            name: 1,
            overview_image:
              'https://res.cloudinary.com/thcx/image/upload/v1616236448/sample.jpg',
          },
          {
            name: 2,
            overview_image:
              'https://res.cloudinary.com/thcx/image/upload/v1616236448/sample.jpg',
          },
          {
            name: 3,
            overview_image:
              'https://res.cloudinary.com/thcx/image/upload/v1616236448/sample.jpg',
          },
          {
            name: 4,
            overview_image:
              'https://res.cloudinary.com/thcx/image/upload/v1616236448/sample.jpg',
          },
          {
            name: 5,
            overview_image:
              'https://res.cloudinary.com/thcx/image/upload/v1616236448/sample.jpg',
          },
          {
            name: 6,
            overview_image:
              'https://res.cloudinary.com/thcx/image/upload/v1616236448/sample.jpg',
          },
          {
            name: 7,
            overview_image:
              'https://res.cloudinary.com/thcx/image/upload/v1616236448/sample.jpg',
          },
          {
            name: 8,
            overview_image:
              'https://res.cloudinary.com/thcx/image/upload/v1616236448/sample.jpg',
          },
        ].map((item, index) => (
          <ListItem button key={index}>
            <ListItemAvatar>
              <Avatar
                src={item?.overview_image}
                alt='Cart-item'
                sx={{}}
                variant='rounded'
              />
              <Box>
                <ListItemText primary={item?.name} />
                <Box>
                  <TextField
                    type='number'
                    inputProps={{ inputMode: 'numeric', pattern: '[0-9]*' }}
                  />
                </Box>
              </Box>
            </ListItemAvatar>
          </ListItem>
        ))}
      </List>
      <Divider />
    </Box>
  )
  return (
    <>
      <Container maxWidth='xl'>
        <Grid container>
          <Grid item sm={12}>
            <Box
              sx={{
                backgroundImage: `url(${bgImg})`,
                height: '100vh',
                // width: '100vw',
                borderBottomRightRadius: '12px',
                borderBottomLeftRadius: '12px',
                backgroundRepeat: 'no-repeat',
                backgroundPosition: 'center',
                backgroundSize: 'cover',
                overflowY: 'hidden',
              }}
            >
              <Box
                sx={{
                  display: 'flex',
                  width: '100%',
                  height: '10%',
                  maxWidth: '80%',
                  margin: '0 auto',
                  justifyContent: 'space-between',
                }}
              >
                <Box sx={{ display: 'flex', alignItems: 'center' }}>
                  <Box sx={{ width: '20%', height: '50%' }}>
                    <img src={logo} alt='logo' />
                  </Box>
                  <Typo
                    variant='h5'
                    style={{
                      color: 'black',
                      width: '250px',
                      fontSize: '16px',
                      fontWeight: 900,
                    }}
                  >
                    MEOW COFFEE
                  </Typo>
                </Box>
                <Box style={{ alignItems: 'center' }}>
                  <List style={{ display: 'flex' }}>
                    <ListItem>
                      <ListItemText sx={classes.sxNavLinkItem}>
                        Menu
                      </ListItemText>
                    </ListItem>
                    <ListItem>
                      <ListItemText sx={classes.sxNavLinkItem}>
                        Trending
                      </ListItemText>
                    </ListItem>
                    <ListItem>
                      <ListItemText sx={classes.sxNavLinkItem}>
                        Voucher
                      </ListItemText>
                    </ListItem>
                  </List>
                </Box>
                <Box
                  sx={{
                    width: '20%',
                    height: '5%',
                    marginTop: '1%',
                    textAlign: 'end',
                  }}
                >
                  <IconButton onClick={toggleDrawer} sx={{}}>
                    <img src={cart} alt='cart' width='50px' height='50px' />
                  </IconButton>
                </Box>
                {list}
                <Drawer
                  anchor={'right'}
                  open={openCart}
                  onClose={() => setOpenCart(false)}
                ></Drawer>
              </Box>
              <Box>
                <Slider />
              </Box>
            </Box>
          </Grid>
        </Grid>
      </Container>
    </>
  )
}

export default ShopViewComponent
