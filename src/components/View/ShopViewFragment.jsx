/** @format */

import { Rating, Typography, Grid, Container, Box } from '@mui/material';
// import { Box } from '@mui/system';
import React from 'react';
import { Swiper, SwiperSlide } from 'swiper/react';
import SwiperCore, { Navigation } from 'swiper/core';
import 'swiper/swiper-bundle.min.css';
import 'swiper/swiper.min.css';
import priceImg from '../../assets/images/priceImg.jpg';
import tasteImg from '../../assets/images/tasteImg.jpg';
import shipImg from '../../assets/images/ship.png';
import coffee from '../../assets/images/mua-coc-giay-cafe.png';
import coffee1 from '../../assets/images/cafe2.jpg';
import useClass from '../../assets/styles/UseClasses';
import { styles } from './ListProductStyle';
SwiperCore.use([Navigation]);

export const Slider = () => {
  const classes = useClass(styles);
  return (
    <>
      <Container maxWidth='xl'>
        <Grid container >
          <Grid item sm={12}>
            <Swiper
              navigation={true}
              spaceBetween={50}
              slidesPerView={1}
              centeredSlides
              onSlideChange={() => console.log('slide change')}
              onSwiper={(swiper) => console.log(swiper)}
            >
              {[
                {
                  name: 'Coffee Cup',
                  overview_image: coffee,
                  // 'https://res.cloudinary.com/thcx/image/upload/v1616236448/sample.jpg',
                  price: 60000,
                  information:
                    'Cà phê chế phin số 5 của Trung Nguyên sau khi pha chế, cà phê có nước pha màu nâu cánh gián đậm, mùi thơm đặc trưng, vị êm nhẹ, ít đắng.\nĐược chế biến từ những hạt cà phê Culi Arabica ngon nhất trên Thế giới cùng những công nghệ hiện đại nhất thế giới và bí quyết riêng không thể sao chép, Chế Phin Số 5 mang hương thơm dịu lưu luyến, vị đậm đà mân mê vị giác chuyên dành cho những người sành cà phê và các quán cà phê muốn sở hữu gu cà phê riêng biệt.',
                },
                {
                  name: 'Coffee Cup',
                  overview_image: coffee1,
                  // 'https://res.cloudinary.com/thcx/image/upload/v1616236448/sample.jpg',
                  price: 60000,
                  information:
                    'Cà phê chế phin số 5 của Trung Nguyên sau khi pha chế, cà phê có nước pha màu nâu cánh gián đậm, mùi thơm đặc trưng, vị êm nhẹ, ít đắng.\nĐược chế biến từ những hạt cà phê Culi Arabica ngon nhất trên Thế giới cùng những công nghệ hiện đại nhất thế giới và bí quyết riêng không thể sao chép, Chế Phin Số 5 mang hương thơm dịu lưu luyến, vị đậm đà mân mê vị giác chuyên dành cho những người sành cà phê và các quán cà phê muốn sở hữu gu cà phê riêng biệt.',
                },
                {
                  name: 'Coffee Cup',
                  overview_image: coffee,
                  // 'https://res.cloudinary.com/thcx/image/upload/v1616236448/sample.jpg',
                  price: 60000,
                  information:
                    'Cà phê chế phin số 5 của Trung Nguyên sau khi pha chế, cà phê có nước pha màu nâu cánh gián đậm, mùi thơm đặc trưng, vị êm nhẹ, ít đắng.\nĐược chế biến từ những hạt cà phê Culi Arabica ngon nhất trên Thế giới cùng những công nghệ hiện đại nhất thế giới và bí quyết riêng không thể sao chép, Chế Phin Số 5 mang hương thơm dịu lưu luyến, vị đậm đà mân mê vị giác chuyên dành cho những người sành cà phê và các quán cà phê muốn sở hữu gu cà phê riêng biệt.',
                },
                {
                  name: 'Coffee Cup',
                  overview_image: coffee1,
                  // 'https://res.cloudinary.com/thcx/image/upload/v1616236448/sample.jpg',
                  price: 60000,
                  information:
                    'Cà phê chế phin số 5 của Trung Nguyên sau khi pha chế, cà phê có nước pha màu nâu cánh gián đậm, mùi thơm đặc trưng, vị êm nhẹ, ít đắng.\nĐược chế biến từ những hạt cà phê Culi Arabica ngon nhất trên Thế giới cùng những công nghệ hiện đại nhất thế giới và bí quyết riêng không thể sao chép, Chế Phin Số 5 mang hương thơm dịu lưu luyến, vị đậm đà mân mê vị giác chuyên dành cho những người sành cà phê và các quán cà phê muốn sở hữu gu cà phê riêng biệt.',
                },
                {
                  name: 'Coffee Cup',
                  overview_image: coffee,
                  // 'https://res.cloudinary.com/thcx/image/upload/v1616236448/sample.jpg',
                  price: 60000,
                  information:
                    'Cà phê chế phin số 5 của Trung Nguyên sau khi pha chế, cà phê có nước pha màu nâu cánh gián đậm, mùi thơm đặc trưng, vị êm nhẹ, ít đắng.\nĐược chế biến từ những hạt cà phê Culi Arabica ngon nhất trên Thế giới cùng những công nghệ hiện đại nhất thế giới và bí quyết riêng không thể sao chép, Chế Phin Số 5 mang hương thơm dịu lưu luyến, vị đậm đà mân mê vị giác chuyên dành cho những người sành cà phê và các quán cà phê muốn sở hữu gu cà phê riêng biệt.',
                },
                {
                  name: 'Coffee Cup',
                  overview_image: coffee1,
                  // 'https://res.cloudinary.com/thcx/image/upload/v1616236448/sample.jpg',
                  price: 60000,
                  information:
                    'Cà phê chế phin số 5 của Trung Nguyên sau khi pha chế, cà phê có nước pha màu nâu cánh gián đậm, mùi thơm đặc trưng, vị êm nhẹ, ít đắng.\nĐược chế biến từ những hạt cà phê Culi Arabica ngon nhất trên Thế giới cùng những công nghệ hiện đại nhất thế giới và bí quyết riêng không thể sao chép, Chế Phin Số 5 mang hương thơm dịu lưu luyến, vị đậm đà mân mê vị giác chuyên dành cho những người sành cà phê và các quán cà phê muốn sở hữu gu cà phê riêng biệt.',
                },
                {
                  name: 'Coffee Cup',
                  overview_image: coffee,
                  // 'https://res.cloudinary.com/thcx/image/upload/v1616236448/sample.jpg',
                  price: 60000,
                  information:
                    'Cà phê chế phin số 5 của Trung Nguyên sau khi pha chế, cà phê có nước pha màu nâu cánh gián đậm, mùi thơm đặc trưng, vị êm nhẹ, ít đắng.\nĐược chế biến từ những hạt cà phê Culi Arabica ngon nhất trên Thế giới cùng những công nghệ hiện đại nhất thế giới và bí quyết riêng không thể sao chép, Chế Phin Số 5 mang hương thơm dịu lưu luyến, vị đậm đà mân mê vị giác chuyên dành cho những người sành cà phê và các quán cà phê muốn sở hữu gu cà phê riêng biệt.',
                },
                {
                  name: 'Coffee Cup',
                  overview_image: coffee1,
                  // 'https://res.cloudinary.com/thcx/image/upload/v1616236448/sample.jpg',
                  price: 60000,
                  information:
                    'Cà phê chế phin số 5 của Trung Nguyên sau khi pha chế, cà phê có nước pha màu nâu cánh gián đậm, mùi thơm đặc trưng, vị êm nhẹ, ít đắng.\nĐược chế biến từ những hạt cà phê Culi Arabica ngon nhất trên Thế giới cùng những công nghệ hiện đại nhất thế giới và bí quyết riêng không thể sao chép, Chế Phin Số 5 mang hương thơm dịu lưu luyến, vị đậm đà mân mê vị giác chuyên dành cho những người sành cà phê và các quán cà phê muốn sở hữu gu cà phê riêng biệt.',
                },
              ].map((item) => {
                return (
                  <SwiperSlide>
                    <Box
                      sx={{
                        height: '90vh',
                        display: 'flex',
                        justifyContent: 'space-evenly',
                      }}
                    >
                      <Box sx={{ marginTop: '2%' }}>
                        <Typography
                          style={{
                            color: 'black',
                            fontWeight: 'bold',
                            fontSize: '52px',
                            marginBottom: '4%',
                            fontWeight: '900'
                          }}
                        >
                          {item?.name}
                        </Typography>
                        <Box
                          style={{
                            display: 'flex',
                            alignItems: 'center',
                            marginBottom: '4%',
                          }}
                        >
                          <Rating value={4.5} readOnly />
                          <Typography
                            style={{
                              color: 'black',
                              fontWeight: 'bold',
                              fontSize: '30px',
                              paddingLeft: '10px'
                            }}
                          >
                            {item?.price} VND
                          </Typography>
                        </Box>
                        <Typography
                          style={{
                            maxWidth: '30rem',
                            // overflow: 'scroll',
                            color: 'black',
                          }}
                        >
                          {item?.information}
                        </Typography>
                        <Typography
                          style={{
                            color: 'black',
                            fontWeight: 'bold',
                            fontSize: '30px',
                            marginBottom: '10%',
                            marginTop: '20px'
                          }}
                        >
                          Features
                        </Typography>
                        <Box style={{ color: "#212529", display: 'flex', justifyContent: 'space-between', fontWeight: 900, fontSize: '24px', textAlign: 'center' }}>
                          <Box sx={{}}>
                            <Box>
                              <img
                                src={priceImg}
                                width={91}
                                height={91}
                                alt='Price'
                              />
                            </Box>
                            <Box>{item?.price}</Box>
                          </Box>
                          <Box>
                            <Box>
                              <img
                                width={91}
                                height={91}
                                src={tasteImg}
                                alt='Taste'
                              />
                            </Box>
                            <Box >6</Box>
                          </Box>
                          <Box>
                            <Box>
                              <img
                                width={91}
                                height={91}
                                src={shipImg}
                                alt='Ship'
                              />
                            </Box>
                            <Box>FREE</Box>
                          </Box>
                        </Box>
                      </Box>
                      <Box
                        sx={{
                          width: '40rem',
                          height: '40rem',
                          marginTop: '2%'
                        }}
                      >
                        <img
                          style={{ borderRadius: '50%' }}
                          src={item?.overview_image}
                          alt='Product-slider'
                        />
                      </Box>
                    </Box>
                  </SwiperSlide>
                );
              })}
            </Swiper>
          </Grid>
        </Grid>
      </Container>
    </>
  );
};
