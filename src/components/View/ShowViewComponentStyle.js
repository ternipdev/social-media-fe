/** @format */

export const styles = (theme) => ({
  sxNavLinkItem: {
    padding: theme.spacing(1.5, 2),
    color: 'black',
    borderRadius: '10px',
    transition: 'all 1000ms',
    cursor: 'pointer',
    ':hover': {
      backgroundColor: '#f48b29',
    },
  }
});
