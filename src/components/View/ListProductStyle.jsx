/** @format */

export const styles = (theme) => ({
  sxProduct: {
    width: '100%',
    height: '350px',
    borderRadius: '10%',
  },
  sximage: {
    width: '100%',
    height: '100%',
    borderRadius: '20px',
  },
  sxtitle: {
    paddingTop: '10px',
    fontSize: '22px',
    fontWeight: 700,
    paddingLeft: '10px'
  },
  sxbutton: {
    padding: '10px 0',
    width:'100%',
    // background: 'white',
    // color: 'black',
    // border: '1px solid #9999',
    fontSize: '16px',
    fontWeight: 800,
    borderRadius: '10px',
    margin: '10px 0',
  },
  sxpriceProduct: {
    display: 'flex',
    padding: '7px 0',
  },
  sxleft: {
    width: '35%',
    fontWeight: 600,
    padding: '0 10px'
  },
  sxright: {
    width: '60%',
    textAlign: 'end',
  },
  sxtitleProduct: {
    margin: '30px 0',
    fontSize: '30px',
    fontWeight: 900,
    textAlign: 'center',
  },
});
