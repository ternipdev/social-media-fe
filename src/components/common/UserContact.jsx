import React from 'react'
import { Grid, Box } from '@mui/material'
import useClasses from '../../assets/styles/UseClasses'
import { styles } from '../../assets/styles/HomeStyles'
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import Avatar from '../../assets/images/cafe2.jpg'
export default function UserContact({ item }) {
    const classes = useClasses(styles)
    return (
        <Grid item sm={12}>
            <Box sx={classes.sxitem}>
                <Box style={{ width: '40px', height: '40px'}}>
                    <img src={item.img} alt="" width='100%' height='100%' style={{borderRadius:'50%'}} />
                </Box>
                <Box sx={classes.sxname}>{item.name}</Box>
            </Box>
        </Grid>
    )
}
