/** @format */

import {
  Box,
  Dialog,
  DialogTitle,
  DialogContent,
  Button,
  Typography as Typo,
  IconButton,
} from '@mui/material'
import useClasses from '../../../assets/styles/UseClasses'
import { styles } from './VoucherStyle'
import React from 'react'
import CloseIcon from '@mui/icons-material/Close'
import bgImg from '../../../assets/images/back-ground.jpg'

function Voucher({ openVoucher, handleCloseVoucher, listVoucher }) {
  const classes = useClasses(styles)

  const _renderVoucher = (voucher, index) => {
    return (
      <Box sx={classes.sxCardVoucher} key={index}>
        <Box sx={classes.sxCardImgWrapper}>
          <img
            className={classes.cardImgWrapper}
            src={voucher?.cover}
            alt={voucher?.title}
          />
        </Box>
        <Box sx={classes.sxCardBody}>
          <Typo sx={classes.sxCardTitle}>{voucher?.title}</Typo>
          <Typo sx={classes.sxCardContent}>
            Description : {voucher?.content}
          </Typo>
          <Typo sx={classes.sxCardContent}>Price : {voucher?.price} coin</Typo>
          <Typo sx={classes.sxCardContent}>HSD : {voucher?.expire} left.</Typo>
          <Typo sx={classes.sxCardContent}>Code : {voucher?.code}</Typo>
        </Box>
        <Box sx={classes.sxCardBottom}>
          <Button
            sx={{
              '&:focus': {
                outline: 'none',
              },
            }}
            variant='contained'
            fullWidth
          >
            Convert
          </Button>
        </Box>
      </Box>
    )
  }
  return (
    <Dialog
      open={openVoucher}
      onClose={handleCloseVoucher}
      aria-labelledby='scroll-dialog-title'
      aria-describedby='scroll-dialog-description'
      fullWidth={true}
      maxWidth='lg'
    >
      <DialogTitle
        id='scroll-dialog-title'
        sx={{ display: 'flex', alignItems: 'center' }}
      >
        <Box sx={{ textAlign: 'center', width: '100%' }}>
          <Typo variant='h6'>Voucher</Typo>
        </Box>
        <IconButton
          sx={{
            '&:focus': {
              outline: 'none',
            },
          }}
          onClick={handleCloseVoucher}
        >
          <CloseIcon sx={{ marginLeft: 'auto' }} />
        </IconButton>
      </DialogTitle>
      <DialogContent
        dividers={true}
        sx={{
          backgroundImage: `url(${bgImg})`,
        }}
      >
        <Box sx={classes.sxCardWrapper}>{listVoucher.map(_renderVoucher)}</Box>
      </DialogContent>
    </Dialog>
  )
}

export default Voucher
