/** @format */

export const styles = (theme) => ({
  sxCardWrapper: {
    display: 'flex',
    flexWrap: 'wrap',
    marginLeft: '-20px',
  },
  sxCardVoucher: {
    width: 'calc(25% - 20px)',
    marginLeft: '20px',
    border: '1px solid',
    padding: '2%',
    borderRadius: '12px',
    marginBottom: '1%',
    backgroundColor: '#fff',
    boxShadow: 'rgba(149, 157, 165, 0.2) 0px 8px 24px',
  },
  sxCardImgWrapper: {
    width: '5rem',
    height: '5rem',
    margin: 'auto',
  },
  cardImg: {
    width: '100%',
    height: '100%',
  },
  sxCardBody: {
    margin: '10% 0',
  },
  sxCardContent: {
    color: '#000',
  },
  sxCardTitle: {
    textTransform: 'uppercase',
    color: '#000',
    fontWeight: 'bold',
    textAlign: 'center',
    marginBottom: '5%',
  },
})
