/** @format */

import React from 'react'
import { Grid, Box } from '@mui/material'
import useClasses from '../../assets/styles/UseClasses'
import { styles } from '../../assets/styles/HomeStyles'
export default function UserContact({ item }) {
  console.log('🚀 ~ file: userGroup.jsx ~ line 6 ~ UserContact ~ item', item)
  const classes = useClasses(styles)
  return (
    <Grid item sm={12}>
      <Box sx={classes.sxitem}>
        <Box>
          <img src={item.img} alt='' width='35px' height='35px'></img>
        </Box>
        <Box sx={classes.sxname}>{item.name}</Box>
      </Box>
    </Grid>
  )
}
