/** @format */

import Routes from './routes'
import React from 'react'
import useMediaQuery from '@mui/material/useMediaQuery'
import { createTheme } from '@mui/material/styles'
import { ThemeProvider as MuiThemeProvider, StylesProvider } from '@mui/styles'
import { ThemeProvider } from '@emotion/react'
import CssBaseline from '@mui/material/CssBaseline'
import '@material-tailwind/react/tailwind.css'
import { colors } from '@mui/material'

function App() {
  const prefersDarkMode = useMediaQuery('(prefers-color-scheme: dark)')

  React.useEffect(() => {
    if (prefersDarkMode) {
      localStorage.setItem('theme', 'dark')
      document.getElementById('theme').classList.add('dark')
    } else {
      document.getElementById('theme').classList.remove('dark')
    }
  }, [prefersDarkMode])

  const theme = React.useMemo(
    (mode) =>
      createTheme({
        palette: {
          mode: prefersDarkMode ? 'dark' : 'light',
          ...(mode === 'light'
            ? {
                // palette values for light mode
                primary: {
                  light: '#ffbc5a',
                  main: '#f48b29',
                  dark: '#bb5d00',
                  contrastText: '#333',
                },
                background: {
                  default: '#f0f2f5',
                  paper: '#ffffff',
                },
              }
            : {
                // palette values for dark mode
                primary: {
                  light: '#ffbc5a',
                  main: '#f48b29',
                  dark: '#bb5d00',
                  contrastText: '#333',
                },
              }),
        },
        shape: {
          borderRadius: 10,
        },
      }),
    [prefersDarkMode],
  )

  return (
    <StylesProvider injectFirst>
      <MuiThemeProvider theme={theme}>
        <ThemeProvider theme={theme}>
          <CssBaseline />
          <div id='theme' className='App w-full min-h-screen'>
            <div className='main w-full m-auto'>
              <Routes />
            </div>
          </div>
        </ThemeProvider>
      </MuiThemeProvider>
    </StylesProvider>
  )
}

export default App
