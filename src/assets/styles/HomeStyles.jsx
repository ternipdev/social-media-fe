/** @format */

export const styles = (theme) => ({
  sxitem: {
    display: 'flex',
    alignItems: 'center',
    width: '100%',
    height: '50px',
    paddingLeft: '10px',
    marginTop: '10px',
    fontWeight: 400,
    '&:hover': {
      backgroundColor: '#ececec',
      borderRadius: '10px',
    },
  },
  sxname: {
    paddingLeft: '10px',
    fontWeight: 500,
  },
  sxTitle: {
    marginTop: '30px',
    fontSize: '20px',
    fontWeight: 800,
  },
})
